<?php

namespace Urjavac\RepoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TicketType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $tipoTicket = array('ayuda' => 'Ayuda', 'error' => 'Error', 'sugerencia' => 'Sugerencia');

        $builder
                ->add('titulo', 'text', array('label' => 'Título del Ticket:', 'required' => true))
                ->add('tipo', 'choice', array('choices' => $tipoTicket, 'label' => 'Tipo de Ticket:', 'empty_value' => 'Elige un tipo de ticket'))
                ->add('descripcion', 'textarea', array('label' => 'Descripción:', 'required' => true))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Ticket'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_ticket';
    }

}
