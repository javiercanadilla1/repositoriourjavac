<?php

namespace Urjavac\RepoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MensajeType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('mensaje', 'textarea', array('label' => 'Comentario:', 'required' => true))
                ->add('comentar', 'submit', array('label' => 'Enviar comentario'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Mensaje'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_mensaje';
    }

}
