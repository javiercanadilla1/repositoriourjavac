<?php

namespace Urjavac\RepoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormatoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', 'text', array('label' => 'Nombre del formato:'))
                ->add('icono', 'text', array('label' => 'Clase css para el icono:'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Formato'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_formato';
    }

}
