<?php

namespace Urjavac\RepoBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FiltrosRecursoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options 
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('radios', 'choice', array('choices' => array('Solo los revisados', 'Solo los oficiales'), 'expanded' => true, 'multiple' => false, 'empty_value' => 'Todos', 'required' => false))
                ->add('asignatura', 'entity', array('class' => "UrjavacRepoBundle:Asignatura", 'property' => 'nombre', 'empty_value' => 'Asignatura...', 'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('a')
                        ->orderBy('a.identificador', 'ASC');
            }))
                ->add('tipo', 'entity', array('class' => "UrjavacRepoBundle:Tiporecurso", 'property' => 'nombre', 'empty_value' => 'Tipo...', 'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('t')
                        ->orderBy('t.nombre', 'ASC');
            }))
                ->add('curso', 'entity', array('class' => "UrjavacRepoBundle:Curso", 'property' => 'curso', 'empty_value' => 'Curso...', 'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                        ->orderBy('c.curso', 'DESC');
            }))
                ->add('formato', 'entity', array('class' => "UrjavacRepoBundle:Formato", 'property' => 'nombre', 'empty_value' => 'Formato...', 'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('f')
                        ->orderBy('f.nombre', 'ASC');
            }))
                ->getForm();
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
//    public function setDefaultOptions(OptionsResolverInterface $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'Urjavac\RepoBundle\Entity\Asignatura'
//        ));
//    }

    public function getName() {
        return 'urjavac_repobundle_filtrosrecurso';
    }

//put your code here
}
