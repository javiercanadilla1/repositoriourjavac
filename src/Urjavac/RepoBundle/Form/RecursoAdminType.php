<?php

namespace Urjavac\RepoBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RecursoAdminType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('idasignatura', 'entity', array('label' => 'Asignatura:', 'class' => 'UrjavacRepoBundle:Asignatura', 'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('a')
                        ->orderBy('a.identificador', 'ASC');
            },))
                ->add('nombre', 'text', array('label' => 'Nombre:', 'required' => true))
                ->add('descripcion', 'textarea', array('max_length' => 140, 'required' => true, 'label' => 'Descripción:'))
                ->add('curso', 'entity', array('label' => 'Curso:', 'class' => 'UrjavacRepoBundle:Curso', 'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                        ->orderBy('c.curso', 'DESC');
            },))
                ->add('tipo', 'entity', array('label' => 'Tipo:', 'class' => 'UrjavacRepoBundle:Tiporecurso'))
                ->add('formato', 'entity', array('label' => 'Formato:', 'class' => 'UrjavacRepoBundle:Formato',
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('f')
                        ->orderBy('f.nombre', 'ASC');
            },))
                ->add('idusuario', 'entity', array('label' => 'Uploader:', 'class' => 'UrjavacRepoBundle:Usuario'))
                ->add('oficial', 'checkbox', array('label' => "¿Es oficial?", 'required' => false))
                ->add('file', 'file', array('required' => false))
                ->add('reset', 'reset', array('label' => 'Reset'))
                ->add('enviar', 'submit', array('label' => 'Guardar'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Recurso',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_recurso';
    }

}
