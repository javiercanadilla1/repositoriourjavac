<?php

namespace Urjavac\RepoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsignaturaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('identificador', 'text', array('label' => 'Identificador de la asignatura:', 'required' => true, 'max_length' => 11))
                ->add('nombre', 'text', array('label' => 'Nombre de la asignatura:', 'required' => true))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Asignatura'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_asignatura';
    }

}
