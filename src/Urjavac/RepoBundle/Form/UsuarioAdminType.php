<?php

namespace Urjavac\RepoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioAdminType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('roles', 'entity', array('class' => 'UrjavacRepoBundle:Role', 'property' => 'nombre', 'multiple' => true))
                ->add('username', 'text', array('label' => 'Nombre de usuario:'))
                ->add('email', 'email', array('label' => 'Email:'))
                ->add('nombre', 'text', array('label' => 'Nombre:'))
                ->add('apellidos', 'text', array('label' => 'Apellidos:'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Urjavac\RepoBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'urjavac_repobundle_usuario';
    }

}
