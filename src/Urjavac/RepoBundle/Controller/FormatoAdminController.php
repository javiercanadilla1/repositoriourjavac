<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Urjavac\RepoBundle\Entity\Formato;
use Urjavac\RepoBundle\Form\FormatoType;

class FormatoAdminController extends Controller {

    public function indexAction(Request $request) {

        $formato = new Formato();

        $form = $this->createForm(new FormatoType(), $formato);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formato);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_formato_index"));
        }

        $formatos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Formato")->findAll();

        return $this->render('UrjavacRepoBundle:FormatoAdmin:index.html.twig', array(
                    'form' => $form->createView(),
                    'formatos' => $formatos
        ));
    }

    public function modificarAction($idFormato, Request $request) {

        $formato = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Formato")->find($idFormato);

        if (!$formato) {
            throw $this->createNotFoundException("No se ha encontrado el formato con id:" . $idFormato);
        }

        $form = $this->createForm(new FormatoType(), $formato);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formato);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_formato_index"));
        }

        return $this->render('UrjavacRepoBundle:FormatoAdmin:modificar.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function eliminarAction($idFormato) {

        $formato = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Formato")->find($idFormato);

        if (!$formato) {
            throw $this->createNotFoundException("No se ha encontrado el formato con id:" . $idFormato);
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($formato);
        $em->flush();

        return $this->redirect($this->generateUrl("admin_formato_index"));
    }

}
