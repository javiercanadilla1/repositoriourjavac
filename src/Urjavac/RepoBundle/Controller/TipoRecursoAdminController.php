<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Urjavac\RepoBundle\Entity\Tiporecurso;
use Urjavac\RepoBundle\Form\TiporecursoType;

class TipoRecursoAdminController extends Controller {

    public function indexAction(Request $request) {

        $tiporecurso = new Tiporecurso();

        $form = $this->createForm(new TiporecursoType(), $tiporecurso);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tiporecurso);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_tiporecurso_index"));
        }

        $tipos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Tiporecurso")->findAll();

        return $this->render('UrjavacRepoBundle:TipoRecursoAdmin:index.html.twig', array(
                    'form' => $form->createView(),
                    'tipos' => $tipos
        ));
    }

    public function modificarAction($idTiporecurso, Request $request) {

        $tiporecurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Tiporecurso")->find($idTiporecurso);

        if (!$tiporecurso) {
            throw $this->createNotFoundException("No se ha encontrado el tipo de recurso con id:" . $idTiporecurso);
        }

        $form = $this->createForm(new TiporecursoType(), $tiporecurso);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tiporecurso);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_tiporecurso_index"));
        }

        return $this->render('UrjavacRepoBundle:TipoRecursoAdmin:modificar.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function eliminarAction($idTiporecurso) {

        $tipoRecurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Tiporecurso")->find($idTiporecurso);


        if (!$tipoRecurso) {
            throw $this->createNotFoundException("No se ha encontrado el tipo de recurso con id:" . $idTiporecurso);
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($tipoRecurso);
        $em->flush();

        return $this->redirect($this->generateUrl("admin_tiporecurso_index"));
    }

}
