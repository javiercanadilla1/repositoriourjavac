<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller {

    public function indexAction() {
        return $this->render('UrjavacRepoBundle:Index:index.html.twig');
    }

}
