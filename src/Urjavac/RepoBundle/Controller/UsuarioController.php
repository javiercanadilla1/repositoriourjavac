<?php

namespace Urjavac\RepoBundle\Controller;

use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Urjavac\RepoBundle\Entity\Usuario;
use Urjavac\RepoBundle\Form\UsuarioType;

class UsuarioController extends Controller {

    public function loginAction(Request $request) {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                        'UrjavacRepoBundle:Usuario:login.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                        )
        );
    }

    public function registrarAction(Request $request) {
        $usuario = new Usuario();
        $form = $this->createForm(new UsuarioType, $usuario);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $role_user = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Role")->findOneByNombre("ROLE_USER");

            $usuario->addRole($role_user);
            $usuario->setIsActive(false);

            //Hash de la contraseña
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($usuario);
            $password = $encoder->encodePassword($usuario->getPassword(), $usuario->getSalt());
            $usuario->setPassword($password);

            //Creamos el codigo de validación
            $cod_validacion = sha1(time());
            $usuario->setCod_validacion($cod_validacion);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            //Enviamos el mensaje al usuario para que active su cuenta
            $this->enviarMensajeActivarUsuario($usuario);

            return $this->render("UrjavacRepoBundle:Usuario:validar.html.twig", array('result' => 'validar'));
        }

        return $this->render("UrjavacRepoBundle:Usuario:registrar.html.twig", array("form" => $form->createView(), "error" => null));
    }

    public function activarAction($idusuario, $token) {
        if (!$idusuario || !$token) {
            $result = "error_null";
        } else {
            $usuario = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->find($idusuario);
            if (!$usuario) {
                return $this->createNotFoundException("No existe el usuario.");
            }
            $result = 'usuario_activo';
            //Existe el usuario -> comprobamos si ya está activado
            if (!$usuario->isEnabled()) {
                $result = 'error_token';
                //Sino está activo comprobamos el Token y el código de validacion
                if ($usuario->getCod_validacion() === $token) {
                    $em = $this->getDoctrine()->getManager();
                    $usuario->setIsActive(true);
                    $em->flush();
                    $result = "activar_ok";
                }
            }
        }
        return $this->render("UrjavacRepoBundle:Usuario:validar.html.twig", array('result' => $result));
    }

    private function enviarMensajeActivarUsuario(Usuario $usuario) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Bienvenido al Repositorio de URJavaC. Por favor, activa tu cuenta.')
                ->setFrom(array('admin@mundoescena.es'=>"admin@mundoescena.es"))
                ->setTo($usuario->getEmail())
                ->setBody($this->renderView('UrjavacRepoBundle:Usuario:email_confirmacion.html.twig', array('idusuario' => $usuario->getId(), 'nombre' => $usuario->getNombre(), 'token' => $usuario->getCod_validacion())), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

}
