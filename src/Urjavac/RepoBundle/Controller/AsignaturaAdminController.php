<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Urjavac\RepoBundle\Entity\Asignatura;
use Urjavac\RepoBundle\Form\AsignaturaType;

class AsignaturaAdminController extends Controller {

    public function crearAction(Request $request) {

        $asignatura = new Asignatura();
        $form = $this->createForm(new AsignaturaType(), $asignatura);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $asignatura->setNrecursos(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($asignatura);
            $em->flush();
            return $this->redirect($this->generateUrl("admin_asignatura_crear"));
        }
        return $this->render("UrjavacRepoBundle:AsignaturaAdmin:crear.html.twig", array('form' => $form->createView()));
    }

    public function listaAction($result) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $asignaturas = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->findBy(array(), array('nombre' => 'ASC'));

        return $this->render("UrjavacRepoBundle:AsignaturaAdmin:lista.html.twig", array('asignaturas' => $asignaturas, 'result' => $result));
    }

    public function modificarAction($idAsignatura, Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        $asignatura = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->find($idAsignatura);
        if (!$asignatura) {
            throw $this->createNotFoundException("No se ha encontrado la asignatura con id:" . $idAsignatura);
        }
        $form = $this->createForm(new AsignaturaType(), $asignatura);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $result = 'modificar_ok';
            return $this->redirect($this->generateUrl("admin_asignatura_lista", array('result' => $result)));
        }
        return $this->render("UrjavacRepoBundle:AsignaturaAdmin:modificar.html.twig", array('form' => $form->createView()));
    }

    public function eliminarAction($idAsignatura) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $asignatura = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->find($idAsignatura);
        if (!$asignatura) {
            throw $this->createNotFoundException("No se ha encontrado la asignatura con id:" . $idAsignatura);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($asignatura);
        $em->flush();

        $result = 'eliminar_ok';
        return $this->redirect($this->generateUrl("admin_asignatura_lista", array('result' => $result)));
    }

}
