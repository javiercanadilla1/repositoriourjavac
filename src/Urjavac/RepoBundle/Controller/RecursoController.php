<?php

namespace Urjavac\RepoBundle\Controller;

use DateTime;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Urjavac\RepoBundle\Entity\Mensaje;
use Urjavac\RepoBundle\Entity\Recurso;
use Urjavac\RepoBundle\Entity\Usuario;
use Urjavac\RepoBundle\Form\FiltrosRecursoType;
use Urjavac\RepoBundle\Form\MensajeType;
use Urjavac\RepoBundle\Form\RecursoType;

/**
 * Recurso controller.
 *
 */
class RecursoController extends Controller {

    public function agregarAction(Request $request, $result) {
        $recurso = new Recurso();
        $form = $this->createForm(new RecursoType(), $recurso);

        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($recurso->getIdusuario() == null) {
                $recurso->setIdusuario($this->getUser());
            }
            $recurso->setRevisado(false)
                    ->setValorPositivo(0)
                    ->setValorNegativo(0);
            $recurso->getIdasignatura()->setNrecursos($recurso->getIdasignatura()->getNrecursos() + 1);
            $recurso->upload();

            $em = $this->getDoctrine()->getManager();
            $em->persist($recurso);
            $em->flush();

            $this->enviarEmailSubida($this->getUser(), $recurso);

            $result = true;
            return $this->redirect($this->generateUrl("recurso_agregar", array('result' => $result)));
        }
        return $this->render("UrjavacRepoBundle:Recurso:agregar.html.twig", array("form" => $form->createView(), "result" => $result));
    }

    public function verTodosAction(Request $request) {

        $form = $this->createForm(new FiltrosRecursoType());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $recursos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->filtrarRecursos($form->getData());
        } else {
            $em = $this->getDoctrine()->getManager();
            $recursos = $em->getRepository('UrjavacRepoBundle:Recurso')->findBy(array(), array("id" => "DESC"));
        }

        return $this->render("UrjavacRepoBundle:Recurso:index.html.twig", array(
                    'recursos' => $recursos, 'form' => $form->createView()));
    }

    public function verPorAsignaturaAction($idasignatura) {
        $em = $this->getDoctrine()->getManager();
        $asignatura = $em->getRepository('UrjavacRepoBundle:Asignatura')->findOneByIdentificador($idasignatura);
        $recursos = $em->getRepository('UrjavacRepoBundle:Recurso')->findByIdasignatura($asignatura);
        return $this->render("UrjavacRepoBundle:Recurso:recurso_asignatura.html.twig", array(
                    'recursos' => $recursos, 'asignatura' => $asignatura
        ));
    }

    public function detallesAction($idrecurso, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $recurso = $em->getRepository('UrjavacRepoBundle:Recurso')->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException('No se ha encontrado el recurso.');
        }
        $mensaje = new Mensaje();

        $form = $this->createForm(new MensajeType(), $mensaje);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $mensaje->setAutor($this->getUser());
            $mensaje->setIdrecurso($recurso);
            date_default_timezone_set("Europe/Madrid");
            $mensaje->setFecha(new DateTime());
            $mensaje->setPuntuacion(0);

            $em->persist($mensaje);
            $em->flush();
            return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $recurso->getId())));
        }

        $listaMensajes = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Mensaje")->findByIdrecurso($recurso);
        return $this->render("UrjavacRepoBundle:Recurso:detalles_recurso.html.twig", array(
                    'recurso' => $recurso, 'form' => $form->createView(), 'listaMensajes' => $listaMensajes
        ));
    }

    public function borrarMensajeAction($idmensaje, $idrecurso) {
        $em = $this->getDoctrine()->getManager();
        //Recuperamos el recurso
        $recurso = $em->getRepository('UrjavacRepoBundle:Recurso')->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException('No se ha encontrado el recurso.');
        }
        if ($this->getUser() !== $recurso->getIdusuario() || !$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        $repositorio_mensajes = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Mensaje");

        if ($idmensaje != null) {
            $mensaje_a_borrar = $repositorio_mensajes->find($idmensaje);

            if (!$mensaje_a_borrar) {
                throw $this->createNotFoundException('No se ha encontrado el mensaje.');
            }

            $em->remove($mensaje_a_borrar);
            $em->flush();
        }

        return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $idrecurso)));
    }

    public function votoPositivoAction($idrecurso) {

        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso con id:" . $idrecurso);
        }

        if ($this->getUser()->hasVotadoNegativo($recurso)) {
            $recurso->setValorNegativo($recurso->getValorNegativo() + 1);
            $recurso->removeVotosNegativosDe($this->getUser());
        }

        if (!$this->getUser()->hasVotadoPositivo($recurso)) {
            $recurso->setValorPositivo($recurso->getValorPositivo() + 1);
            $recurso->addVotosPositivosDe($this->getUser());
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($recurso);
        $em->flush();

        return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $idrecurso)));
    }

    public function votoNegativoAction($idrecurso) {
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso con id:" . $idrecurso);
        }

        if ($this->getUser()->hasVotadoPositivo($recurso)) {
            $recurso->removeVotosPositivosDe($this->getUser());
            $recurso->setValorPositivo($recurso->getValorPositivo() - 1);
        }

        if (!$this->getUser()->hasVotadoNegativo($recurso)) {
            $recurso->setValorNegativo($recurso->getValorNegativo() - 1);
            $recurso->addVotosNegativosDe($this->getUser());
        }

        $this->getDoctrine()->getManager()->persist($recurso);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $idrecurso)));
    }

    public function quitarVotoPositivoAction($idrecurso) {
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso con id:" . $idrecurso);
        }

        $recurso->setValorPositivo($recurso->getValorPositivo() - 1);
        $recurso->removeVotosPositivosDe($this->getUser());

        $this->getDoctrine()->getManager()->persist($recurso);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $idrecurso)));
    }

    public function quitarVotoNegativoAction($idrecurso) {
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso con id:" . $idrecurso);
        }

        $recurso->setValorNegativo($recurso->getValorNegativo() + 1);
        $recurso->removeVotosNegativosDe($this->getUser());

        $this->getDoctrine()->getManager()->persist($recurso);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl("recurso_detalles", array('idrecurso' => $idrecurso)));
    }

    private function enviarEmailSubida(Usuario $usuario, Recurso $recurso) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Nuevo recurso subido:' . $recurso->getNombre())
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo("admin@mundoescena.es")
                ->setBody($this->renderView('UrjavacRepoBundle:Recurso:email_nuevo_recurso_admin.html.twig', array('usuario' => $usuario, 'recurso' => $recurso)), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

}
