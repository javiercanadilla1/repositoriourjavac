<?php

namespace Urjavac\RepoBundle\Controller;

use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Urjavac\RepoBundle\Entity\Usuario;
use Urjavac\RepoBundle\Form\UsuarioAdminType;
use Urjavac\RepoBundle\Form\UsuarioType;

class UsuarioAdminController extends Controller {

    public function crearAction(Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $usuario = new Usuario();
        $form = $this->createForm(new UsuarioType(), $usuario);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $password_text = $usuario->getPassword();

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($usuario);
            $password = $encoder->encodePassword($usuario->getPassword(), $usuario->getSalt());

            $usuario->addRole($em->getRepository("UrjavacRepoBundle:Role")->findOneByNombre("ROLE_USER"));
            $usuario->setIsActive(false)
                    ->setPassword($password);

            $usuario->setCod_validacion(sha1(time()));

            $em->persist($usuario);
            $em->flush();

            $this->enviarMensajeActivarUsuario($usuario, $password_text);

            return $this->redirect($this->generateUrl("admin_usuario_crear"));
        }
        return $this->render("UrjavacRepoBundle:UsuarioAdmin:crear.html.twig", array("form" => $form->createView()));
    }

    public function listaAction($result) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $usuarios = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->findAll();

        return $this->render("UrjavacRepoBundle:UsuarioAdmin:lista.html.twig", array('usuarios' => $usuarios, 'result' => $result));
    }

    public function modificarAction($idUsuario, Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $usuario = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->find($idUsuario);
        if (!$usuario) {
            throw $this->createNotFoundException("No se ha encontrado el usuario con idUsuario=" . $idUsuario);
        }
        $form = $this->createForm(new UsuarioAdminType(), $usuario);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            return $this->redirect($this->generateUrl("admin_usuario_lista", array('result' => 'modificar_ok')));
        }

        return $this->render("UrjavacRepoBundle:UsuarioAdmin:modificar.html.twig", array('form' => $form->createView()));
    }

    public function eliminarAction($idUsuario) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        if ($idUsuario != null) {
            $usuario = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->find($idUsuario);
            if (!$usuario) {
                throw $this->createNotFoundException("No se ha encontrado el usuario con id:" . $idUsuario);
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($usuario);
            $em->flush();
            return $this->redirect($this->generateUrl("admin_usuario_lista", array('result' => 'eliminar_ok')));
        }

        return $this->redirect($this->generateUrl("admin_usuario_lista", array('result' => null)));
    }

    public function activarAction($idUsuario, $estado) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        if ($idUsuario != null) {
            $usuario = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->find($idUsuario);
            if (!$usuario) {
                throw $this->createNotFoundException("No se ha encontrado el usuario con id:" . $idUsuario);
            }
            $usuario->setIsActive(($estado == 1) ? false : true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
        }
        return $this->redirect($this->generateUrl("admin_usuario_lista", array('result' => null)));
    }

    private function enviarMensajeActivarUsuario(Usuario $usuario, $password) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Bienvenido al Repositorio de URJavaC. Por favor, activa tu cuenta.')
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo($usuario->getEmail())
                ->setBody($this->renderView('UrjavacRepoBundle:UsuarioAdmin:email_confirmacion.html.twig', array('usuario' => $usuario, 'password' => $password)), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

}
