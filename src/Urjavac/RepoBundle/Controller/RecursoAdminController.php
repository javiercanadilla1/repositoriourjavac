<?php

namespace Urjavac\RepoBundle\Controller;

use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Urjavac\RepoBundle\Entity\Asignatura;
use Urjavac\RepoBundle\Entity\Recurso;
use Urjavac\RepoBundle\Form\FiltrosRecursoAdminType;
use Urjavac\RepoBundle\Form\RecursoAdminType;

class RecursoAdminController extends Controller {

    public function crearAction(Request $request, $result) {
        $recurso = new Recurso();
        $form = $this->createForm(new RecursoAdminType(), $recurso);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $recurso->setRevisado(true)
                    ->setValorPositivo(0)
                    ->setValorNegativo(0);
            $asignatura = $recurso->getIdAsignatura();
            $asignatura->setNrecursos($asignatura->getNrecursos() + 1);
            $recurso->upload();

            $em = $this->getDoctrine()->getManager();
            $em->persist($recurso);
            $em->flush();

            $this->enviarEmailAsignaturasFavoritas($recurso, $asignatura);

            $result = true;
            return $this->redirect($this->generateUrl("admin_recurso_crear", array('result' => $result)));
        }
        return $this->render("UrjavacRepoBundle:RecursoAdmin:crear.html.twig", array("form" => $form->createView(), "result" => $result));
    }

    public function listaAction($result, Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(new FiltrosRecursoAdminType());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $listaRecursos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->filtrarRecursos($form->getData());
        } else {
            $listaRecursos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->findBy(array(), array("id" => "DESC"));
        }

        return $this->render("UrjavacRepoBundle:RecursoAdmin:lista.html.twig", array('recursos' => $listaRecursos, 'result' => $result, 'form' => $form->createView()));
    }

    public function cambiarEstadoAction($idrecurso, $estado) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idrecurso);

        if (!$recurso) {
            throw $this->createNotFoundException('No se ha encontrado el recurso a modificar.');
        }
        $em = $this->getDoctrine()->getManager();

        if ($estado) {
            $recurso->setRevisado(false);
            $em->flush();
        } else {
            $recurso->setRevisado(true);
            $em->flush();
            $this->enviarEmailAsignaturasFavoritas($recurso, $recurso->getIdAsignatura());
        }
        return $this->redirect($this->generateUrl("admin_recurso_lista", array('result' => null)));
    }

    public function modificarAction($idRecurso, Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException ();
        }
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idRecurso);
        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso a modificar.");
        }
        $form = $this->createForm(new RecursoAdminType(), $recurso);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //Si se ha introducido un nuevo archivo se guarda sino no.
            if ($form->get('file')->getData() != null) {
                $recurso->upload();
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $result = "modificar_ok";
            return $this->redirect($this->generateUrl("admin_recurso_lista", array('result' => $result)));
        }
        return $this->render("UrjavacRepoBundle:RecursoAdmin:modificar.html.twig", array('form' => $form->createView()));
    }

    public function eliminarAction($idRecurso) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $recurso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->find($idRecurso);
        if (!$recurso) {
            throw $this->createNotFoundException("No se ha encontrado el recurso a eliminar.");
        }
        $em = $this->getDoctrine()->getManager();
        $recurso->removeUpload();
        $recurso->getIdasignatura()->setNrecursos($recurso->getIdasignatura()->getNrecursos() - 1);
        $em->remove($recurso);
        $em->flush();

        $result = "eliminar_ok";
        return $this->redirect($this->generateUrl("admin_recurso_lista", array('result' => $result)));
    }

    private function enviarEmailAsignaturasFavoritas(Recurso $recurso, Asignatura $asignatura) {
        foreach ($asignatura->getFavoritaDe() as $usuario) {
            $mensaje = Swift_Message::newInstance();
            $mensaje->setSubject('Nuevo recurso en la asignatura:' . $asignatura->getNombre())
                    ->setFrom(array("admin@mundoescena.es" => "admin@mundoescena.es"))
                    ->setTo($usuario->getEmail())
                    ->setBody($this->renderView('UrjavacRepoBundle:RecursoAdmin:email_nuevo_recurso.html.twig', array('recurso' => $recurso, 'asignatura' => $asignatura)), 'text/html', 'UTF-8');
            $this->get('swiftmailer.mailer.spool')->send($mensaje);
        }
    }

}
