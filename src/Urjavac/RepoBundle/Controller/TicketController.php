<?php

namespace Urjavac\RepoBundle\Controller;

use DateTime;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Urjavac\RepoBundle\Entity\RespuestaTicket;
use Urjavac\RepoBundle\Entity\Ticket;
use Urjavac\RepoBundle\Entity\Usuario;
use Urjavac\RepoBundle\Form\RespuestaTicketType;
use Urjavac\RepoBundle\Form\TicketType;

class TicketController extends Controller {

    public function nuevoAction(Request $request) {
        $ticket = new Ticket();
        $form = $this->createForm(new TicketType(), $ticket);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $ticket->setFechaHora(new DateTime());
            $ticket->setEstado("abierto");
            $ticket->setIdUsuario($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();

            $this->enviarMensajeTicketUsuario($this->getUser(), $ticket);
            $this->enviarMensajeTicketAdministradores($this->getUser(), $ticket);

            return $this->render('UrjavacRepoBundle:Ticket:confirmar.html.twig');
        }

        return $this->render('UrjavacRepoBundle:Ticket:nuevo.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function estadoAction($idTicket, Request $request) {

        $ticket = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->find($idTicket);

        if (!$ticket) {
            throw $this->createNotFoundException("No se ha encontrado el ticket con id: " . $idTicket);
        }

        $respuesta = new RespuestaTicket();

        $form = $this->createForm(new RespuestaTicketType(), $respuesta);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $respuesta->setIdUsuario($this->getUser())
                    ->setFechaHora(new DateTime());

            $respuesta->setTicket($ticket);

            $em = $this->getDoctrine()->getManager();
            $em->persist($respuesta);
            $em->flush();
            return $this->redirect($this->generateUrl("ticket_estado", array('idTicket' => $ticket->getId())));
        }

        return $this->render('UrjavacRepoBundle:Ticket:estado.html.twig', array(
                    'form' => $form->createView(),
                    'ticket' => $ticket
        ));
    }

    public function indexAction() {
        $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByIdUsuario($this->getUser());
        return $this->render('UrjavacRepoBundle:Ticket:index.html.twig', array(
                    'tickets' => $tickets
        ));
    }

    private function enviarMensajeTicketUsuario(Usuario $usuario, Ticket $ticket) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Nuevo ticket abierto:' . $ticket->getTitulo())
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo($usuario->getEmail())
                ->setBody($this->renderView('UrjavacRepoBundle:Ticket:email_nuevo_ticket.html.twig', array('idTicket' => $ticket->getId())), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

    private function enviarMensajeTicketAdministradores(Usuario $usuario, Ticket $ticket) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Nuevo ticket abierto:' . $ticket->getTitulo())
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo("admin@mundoescena.es")
                ->setBody($this->renderView('UrjavacRepoBundle:Ticket:email_nuevo_ticket_admin.html.twig', array('usuario' => $usuario, 'ticket' => $ticket)), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

}
