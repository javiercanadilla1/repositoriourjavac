<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Urjavac\RepoBundle\Entity\Usuario;

class AsignaturaController extends Controller {

    public function indexAction() {
        $asignaturas = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->findBy(array(), array('nombre' => 'ASC'));
        return $this->render("UrjavacRepoBundle:Asignatura:index.html.twig", array("asignaturas" => $asignaturas));
    }

    public function marcarFavoritaAction($idAsignatura, $estado) {
        $asignatura = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->find($idAsignatura);

        if (!$asignatura) {
            throw $this->createNotFoundException("No se ha encontrado la asignatura con id:" . $idAsignatura);
        }

        $usuario = $this->getUser();
        if ($estado) {
            $usuario->removeAsignaturasFavorita($asignatura);
        } else {
            $usuario->addAsignaturasFavorita($asignatura);
        }

        $this->getDoctrine()->getManager()->persist($usuario);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl("asignatura_ver_todas"));
    }

}
