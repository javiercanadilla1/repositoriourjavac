<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdministradoresController extends Controller {

    public function indexAction() {
        $administradores = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->findByNombreRole("ROLE_ADMIN");

        return $this->render("UrjavacRepoBundle:Administradores:index.html.twig", array('administradores' => $administradores));
    }

}
