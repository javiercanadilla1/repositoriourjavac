<?php

namespace Urjavac\RepoBundle\Controller;

use DateTime;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Urjavac\RepoBundle\Entity\RespuestaTicket;
use Urjavac\RepoBundle\Entity\Usuario;
use Urjavac\RepoBundle\Entity\Ticket;
use Urjavac\RepoBundle\Form\RespuestaTicketType;

class TicketAdminController extends Controller {

    public function vistaAction($idTicket, Request $request) {
        $ticket = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->find($idTicket);
        if (!$ticket) {
            throw $this->createNotFoundException("No se ha encontrado el ticket con id:" . $idTicket);
        }

        $respuesta = new RespuestaTicket();

        $form = $this->createForm(new RespuestaTicketType(), $respuesta);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $respuesta->setIdUsuario($this->getUser())
                    ->setFechaHora(new DateTime());

            $respuesta->setTicket($ticket);

            $this->getDoctrine()->getManager()->persist($respuesta);
            $this->getDoctrine()->getManager()->flush();

            $this->enviarMensajeTicketUsuario($ticket->getIdUsuario(), $ticket, $respuesta);

            return $this->redirect($this->generateUrl("admin_ticket_lista"));
        }

        return $this->render('UrjavacRepoBundle:TicketAdmin:vista.html.twig', array(
                    'ticket' => $ticket,
                    'form' => $form->createView()
        ));
    }

    public function listaAction($carpeta) {
        switch ($carpeta) {
            case "abiertos":
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByEstado("abierto", array('fechaHora' => 'desc'));
                break;
            case "ayuda":
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByTipo("ayuda", array('fechaHora' => 'desc'));
                break;
            case "error":
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByTipo("error", array('fechaHora' => 'desc'));
                break;
            case "sugerencia":
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByTipo("sugerencia", array('fechaHora' => 'desc'));
                break;
            case "cerrados":
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findByEstado("cerrado", array('fechaHora' => 'desc'));
                break;
            default:
                $tickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->findBy(array(), array('fechaHora' => 'desc'));
        }

        return $this->render('UrjavacRepoBundle:TicketAdmin:lista.html.twig', array(
                    'tickets' => $tickets
        ));
    }

    public function eliminarAction($idTicket) {

        $ticket = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->find($idTicket);

        if (!$ticket) {
            throw $this->createNotFoundException("No se ha encontrado el ticket con id:" . $idTicket);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($ticket);
        $em->flush();

        return $this->redirect($this->generateUrl("admin_ticket_lista"));
    }

    public function cambiarEstadoAction($idTicket) {

        $ticket = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->find($idTicket);

        if (!$ticket) {
            throw $this->createNotFoundException("No se ha encontrado el ticket con id:" . $idTicket);
        }

        $ticket->setEstado(($ticket->getEstado() == "abierto") ? "cerrado" : "abierto");

        $this->getDoctrine()->getManager()->persist($ticket);
        $this->getDoctrine()->getManager()->flush();

        $this->enviarCambioEstadoTicketUsuario($ticket->getIdUsuario(), $ticket);

        return $this->redirect($this->generateUrl("admin_ticket_lista"));
    }

    private function enviarMensajeTicketUsuario(Usuario $usuario, Ticket $ticket, RespuestaTicket $respuesta) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Nueva respuesta en el ticket:' . $ticket->getTitulo())
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo($usuario->getEmail())
                ->setBody($this->renderView('UrjavacRepoBundle:Ticket:email_respuesta_ticket.html.twig', array('ticket' => $ticket, 'respuesta' => $respuesta)), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

    private function enviarCambioEstadoTicketUsuario(Usuario $usuario, Ticket $ticket) {
        $mensaje = Swift_Message::newInstance();
        $mensaje->setSubject('Cambio de estado en el ticket:' . $ticket->getTitulo())
                ->setFrom(array('admin@mundoescena.es' => "admin@mundoescena.es"))
                ->setTo($usuario->getEmail())
                ->setBody($this->renderView('UrjavacRepoBundle:Ticket:email_cambio_estado_ticket.html.twig', array('ticket' => $ticket)), 'text/html', 'UTF-8');
        $this->get('mailer')->send($mensaje);
    }

}
