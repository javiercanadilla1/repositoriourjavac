<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexAdminController extends Controller {

    public function indexAction() {
        /** USUARIOS * */
        $numUsuarios = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->countAll();
        $numUsuariosActivos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->countActivos();
        $numUsuariosNoActivos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->countNoActivos();
        $numUsuariosAdministradores = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Usuario")->countAdminitradores();

        /** RECURSOS * */
        $numRecursos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->countAll();
        $numRecursosRevisados = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->countRevisados();
        $numRecursosNoRevisados = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->countNoRevisados();
        $numRecursosOficiales = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Recurso")->countOficiales();

        /** TICKETS * */
        $numTickets = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->countAll();
        $numTicketsAbiertos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->countAbiertos();
        $numTicketsCerrados = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Ticket")->countCerrados();

        /** OTRA INFORMACIÓN * */
        $numAsignaturas = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Asignatura")->countAll();
        $numRoles = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Role")->countAll();

        return $this->render("UrjavacRepoBundle:IndexAdmin:index.html.twig", array('numUsuarios' => $numUsuarios,
                    'numUsuariosActivos' => $numUsuariosActivos,
                    'numUsuariosNoActivos' => $numUsuariosNoActivos,
                    'numUsuariosAdministradores' => $numUsuariosAdministradores,
                    'numRecursos' => $numRecursos,
                    'numRecursosRevisados' => $numRecursosRevisados,
                    'numRecursosNoRevisados' => $numRecursosNoRevisados,
                    'numRecursosOficiales' => $numRecursosOficiales,
                    'numTickets' => $numTickets,
                    'numTicketsAbiertos' => $numTicketsAbiertos,
                    'numTicketsCerrados' => $numTicketsCerrados,
                    'numAsignaturas' => $numAsignaturas,
                    'numRoles' => $numRoles
        ));
    }

}
