<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Urjavac\RepoBundle\Entity\Curso;
use Urjavac\RepoBundle\Form\CursoType;

class CursosAdminController extends Controller {

    public function indexAction(Request $request) {

        $curso = new Curso();

        $form = $this->createForm(new CursoType(), $curso);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($curso);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_curso_index"));
        }

        $cursos = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Curso")->findAll();


        return $this->render('UrjavacRepoBundle:CursosAdmin:index.html.twig', array(
                    'cursos' => $cursos,
                    'form' => $form->createView()
        ));
    }

    public function modificarAction($idCurso, Request $request) {

        $curso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Curso")->find($idCurso);

        if (!$curso) {
            throw $this->createNotFoundException("No se ha encontrado el curso con id:" . $idCurso);
        }

        $form = $this->createForm(new CursoType(), $curso);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($curso);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_curso_index"));
        }

        return $this->render('UrjavacRepoBundle:CursosAdmin:modificar.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function eliminarAction($idCurso) {

        $curso = $this->getDoctrine()->getRepository("UrjavacRepoBundle:Curso")->find($idCurso);


        if (!$curso) {
            throw $this->createNotFoundException("No se ha encontrado el curso con id:" . $idCurso);
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($curso);
        $em->flush();

        return $this->redirect($this->generateUrl("admin_curso_index"));
    }

}
