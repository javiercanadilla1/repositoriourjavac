<?php

namespace Urjavac\RepoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Urjavac\RepoBundle\Entity\Role;
use Urjavac\RepoBundle\Form\RoleType;

class RolesAdminController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $rol = new Role();

        $form = $this->createForm(new RoleType(), $rol);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($rol);
            $em->flush();
        }

        $roles = $em->getRepository("UrjavacRepoBundle:Role")->findAll();

        return $this->render("UrjavacRepoBundle:RolesAdmin:index.html.twig", array('form' => $form->createView(), 'roles' => $roles));
    }

    public function modificarAction($idRol, Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $rol = $em->getRepository("UrjavacRepoBundle:Role")->find($idRol);
        if (!$rol) {
            throw $this->createNotFoundException("No se ha encontrado el rol con idRol=" . $idRol);
        }
        $form = $this->createForm(new RoleType(), $rol);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($rol);
            $em->flush();
            return $this->redirect($this->generateUrl("admin_roles_index"));
        }
        return $this->render("UrjavacRepoBundle:RolesAdmin:modificar.html.twig", array('form' => $form->createView()));
    }

    public function eliminarAction($idRol) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $rol = $em->getRepository("UrjavacRepoBundle:Role")->find($idRol);
        if (!$rol) {
            throw $this->createNotFoundException("No se ha encontrado el rol con idRol=" . $idRol);
        } else {
            $em->remove($rol);
            $em->flush();
        }
        return $this->redirect($this->generateUrl("admin_roles_index"));
    }

}
