<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TicketRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TicketRepository extends EntityRepository {

    public function countAll() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(t.id)')
                        ->from("UrjavacRepoBundle:Ticket", 't')
                        ->getQuery()->getSingleScalarResult();
    }

    public function countAbiertos() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(t.id)')
                        ->from("UrjavacRepoBundle:Ticket", 't')
                        ->where("t.estado = :estado")
                        ->setParameter("estado", "abierto")
                        ->getQuery()->getSingleScalarResult();
    }

    public function countCerrados() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(t.id)')
                        ->from("UrjavacRepoBundle:Ticket", 't')
                        ->where("t.estado = :estado")
                        ->setParameter("estado", "cerrado")
                        ->getQuery()->getSingleScalarResult();
    }

}
