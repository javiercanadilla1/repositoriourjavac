<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tiporecurso
 *
 * @ORM\Table(name="repo_tiporecurso", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_DE3126FF3A909126", columns={"nombre"})})
 * @ORM\Entity
 */
class Tiporecurso {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     * @ORM\OneToMany(targetEntity="Recurso", mappedBy="tipo")
     */
    protected $nombre;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tiporecurso
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    public function __toString() {
        return $this->nombre;
    }

}
