<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formato
 *
 * @ORM\Table(name="repo_formato", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_19BDE6733A909126", columns={"nombre"})})
 * @ORM\Entity
 */
class Formato {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     * @ORM\OneToMany(targetEntity="Recurso", mappedBy="nombre")
     */
    protected $nombre;

    /**
     * @var String
     * @ORM\Column(name="icono", type="string", length=255, nullable=false)
     */
    protected $icono;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Formato
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    public function __toString() {
        return $this->nombre;
    }

    /**
     * Set icono
     *
     * @param string $icono
     * @return Formato
     */
    public function setIcono($icono) {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return string
     */
    public function getIcono() {
        return $this->icono;
    }

}
