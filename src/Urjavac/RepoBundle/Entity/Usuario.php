<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Usuario
 *
 * @ORM\Table(name="repo_usuario")
 * @ORM\Entity(repositoryClass="UsuarioRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 *
 */
class Usuario implements AdvancedUserInterface, Serializable {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false, unique=true)
     * @Assert\NotNull()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\NotNull()
     */
    private $password;

    /**
     * @var string
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=255, nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=100, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $apellidos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="idUsuario")
     * @ORM\JoinTable(name="user_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_validacion", type="string", nullable=true)
     */
    private $cod_validacion;

    /**
     * @ORM\ManyToMany(targetEntity="Recurso", mappedBy="votosPositivosDe")
     *
     */
    protected $votadoPositivo;

    /**
     * @ORM\ManyToMany(targetEntity="Recurso", mappedBy="votosNegativosDe")
     *
     */
    protected $votadoNegativo;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="idUsuario")
     *
     */
    protected $tickets;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Asignatura", inversedBy="favoritaDe")
     * @ORM\JoinTable(name="repo_asignaturasFavoritas_usuarios")
     */
    protected $asignaturasFavoritas;

    public function __construct() {
        $this->roles = new ArrayCollection();
        $this->salt = (md5(time()));
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
        return $this;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
        return $this;
    }

    public function eraseCredentials() {

    }

    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        return $this->roles->toArray();
    }

    public function addRole(Role $role) {
        $this->roles[] = $role;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
        return $this;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
        return $this;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getCod_validacion() {
        return $this->cod_validacion;
    }

    public function setCod_validacion($cod_validacion) {
        $this->cod_validacion = $cod_validacion;
        return $this;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        return $this->isActive;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            $this->salt,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                $this->salt
                ) = unserialize($serialized);
    }

    /**
     * Remove roles
     *
     * @param Role $roles
     */
    public function removeRole(Role $roles) {
        $this->roles->removeElement($roles);
        return $this;
    }

    public function __toString() {
        return $this->username;
    }

    /**
     * @Assert\True(message="El email introducido no es un email universitario. Introduce un email de alumno de la URJC")
     */
    public function isEmailUniversitario() {
        return (strrpos($this->getEmail(), "@alumnos.urjc.es") != false) ? true : false;
    }

    /**
     * Set cod_validacion
     *
     * @param string $codValidacion
     * @return Usuario
     */
    public function setCodValidacion($codValidacion) {
        $this->cod_validacion = $codValidacion;

        return $this;
    }

    /**
     * Get cod_validacion
     *
     * @return string
     */
    public function getCodValidacion() {
        return $this->cod_validacion;
    }

    /**
     * Add tickets
     *
     * @param Ticket $tickets
     * @return Usuario
     */
    public function addTicket(Ticket $tickets) {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param Ticket $tickets
     */
    public function removeTicket(Ticket $tickets) {
        $this->tickets->removeElement($tickets);
        return $this;
    }

    /**
     * Get tickets
     *
     * @return Collection
     */
    public function getTickets() {
        return $this->tickets;
    }

    /**
     * Add asignaturasFavoritas
     *
     * @param Asignatura $asignaturasFavoritas
     * @return Usuario
     */
    public function addAsignaturasFavorita(Asignatura $asignaturasFavoritas) {
        $this->asignaturasFavoritas[] = $asignaturasFavoritas;

        return $this;
    }

    /**
     * Remove asignaturasFavoritas
     *
     * @param Asignatura $asignaturasFavoritas
     */
    public function removeAsignaturasFavorita(Asignatura $asignaturasFavoritas) {
        $this->asignaturasFavoritas->removeElement($asignaturasFavoritas);
    }

    /**
     * Get asignaturasFavoritas
     *
     * @return Collection
     */
    public function getAsignaturasFavoritas() {
        return $this->asignaturasFavoritas;
    }

    /**
     *
     * @param Asignatura $asignatura
     * @return TRUE or FALSE
     */
    public function hasAsignaturaFavorita(Asignatura $asignatura) {
        return in_array($asignatura, $this->asignaturasFavoritas, true);
    }

    /**
     * Add votadoPositivo
     *
     * @param Recurso $votadoPositivo
     * @return Usuario
     */
    public function addVotadoPositivo(Recurso $votadoPositivo) {
        $this->votadoPositivo[] = $votadoPositivo;

        return $this;
    }

    /**
     * Remove votadoPositivo
     *
     * @param Recurso $votadoPositivo
     */
    public function removeVotadoPositivo(Recurso $votadoPositivo) {
        $this->votadoPositivo->removeElement($votadoPositivo);
        return $this;
    }

    /**
     * Get votadoPositivo
     *
     * @return Collection
     */
    public function getVotadoPositivo() {
        return $this->votadoPositivo;
    }

    /**
     *
     * @param Recurso $recurso
     * @return True or False
     */
    public function hasVotadoPositivo(Recurso $recurso) {
        return in_array($recurso, $this->votadoPositivo->toArray(), true);
    }

    /**
     * Add votadoNegativo
     *
     * @param Recurso $votadoNegativo
     * @return Usuario
     */
    public function addVotadoNegativo(Recurso $votadoNegativo) {
        $this->votadoNegativo[] = $votadoNegativo;

        return $this;
    }

    /**
     * Remove votadoNegativo
     *
     * @param Recurso $votadoNegativo
     */
    public function removeVotadoNegativo(Recurso $votadoNegativo) {
        $this->votadoNegativo->removeElement($votadoNegativo);
        return $this;
    }

    /**
     * Get votadoNegativo
     *
     * @return Collection
     */
    public function getVotadoNegativo() {
        return $this->votadoNegativo;
    }

    public function hasVotadoNegativo(Recurso $recurso) {
        return in_array($recurso, $this->votadoNegativo->toArray(), true);
    }

}
