<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curso
 *
 * @ORM\Table(name="repo_curso", uniqueConstraints={@ORM\UniqueConstraint(name="curso", columns={"curso"})})
 * @ORM\Entity
 */
class Curso {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\OneToMany(targetEntity="Recurso", mappedBy="curso")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="curso", type="string", length=10, nullable=false, unique=true)
     *
     */
    protected $curso;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set curso
     *
     * @param string $curso
     * @return Curso
     */
    public function setCurso($curso) {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso
     *
     * @return string
     */
    public function getCurso() {
        return $this->curso;
    }

    public function __toString() {
        return $this->curso;
    }

}
