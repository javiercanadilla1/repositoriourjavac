<?php

namespace Urjavac\RepoBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="repo_ticket")
 * @ORM\Entity(repositoryClass="TicketRepository")
 */
class Ticket {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Ticket
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="tickets")
     * @ORM\JoinColumn(name="idUsuario", referencedColumnName="id")
     */
    private $idUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fechaHora;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=10)
     */
    private $estado;

    /**
     *
     * @var string
     * @ORM\Column(name="tipo", type="string", length=20)
     */
    private $tipo;

    /**
     *
     * @var Ticket
     * @ORM\OneToMany(targetEntity="RespuestaTicket", mappedBy="ticket")
     *
     */
    protected $respuestas;

    /**
     * Constructor
     */
    public function __construct() {
        $this->respuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Ticket
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Ticket
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set fechaHora
     *
     * @param \DateTime $fechaHora
     * @return Ticket
     */
    public function setFechaHora($fechaHora) {
        $this->fechaHora = $fechaHora;

        return $this;
    }

    /**
     * Get fechaHora
     *
     * @return \DateTime
     */
    public function getFechaHora() {
        return $this->fechaHora;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Ticket
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Ticket
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set idUsuario
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $idUsuario
     * @return Ticket
     */
    public function setIdUsuario(\Urjavac\RepoBundle\Entity\Usuario $idUsuario = null) {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Urjavac\RepoBundle\Entity\Usuario
     */
    public function getIdUsuario() {
        return $this->idUsuario;
    }

    /**
     * Add respuestas
     *
     * @param \Urjavac\RepoBundle\Entity\RespuestaTicket $respuestas
     * @return Ticket
     */
    public function addRespuesta(\Urjavac\RepoBundle\Entity\RespuestaTicket $respuestas) {
        $this->respuestas[] = $respuestas;

        return $this;
    }

    /**
     * Remove respuestas
     *
     * @param \Urjavac\RepoBundle\Entity\RespuestaTicket $respuestas
     */
    public function removeRespuesta(\Urjavac\RepoBundle\Entity\RespuestaTicket $respuestas) {
        $this->respuestas->removeElement($respuestas);
    }

    /**
     * Get respuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespuestas() {
        return $this->respuestas;
    }

}
