<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mensaje
 *
 * @ORM\Table(name="repo_mensaje", indexes={@ORM\Index(name="IDX_9B631D0131075EBA", columns={"autor"})})
 * @ORM\Entity
 */
class Mensaje {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    protected $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="text", nullable=false)
     */
    protected $mensaje;

    /**
     * @var integer
     *
     * @ORM\Column(name="puntuacion", type="smallint", nullable=false)
     */
    protected $puntuacion;

    /**
     * @var \Urjavac\RepoBundle\Entity\Recurso
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Recurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idRecurso", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $idrecurso;

    /**
     * @var \Urjavac\RepoBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="autor", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $autor;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Mensaje
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     * @return Mensaje
     */
    public function setMensaje($mensaje) {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string
     */
    public function getMensaje() {
        return $this->mensaje;
    }

    /**
     * Set puntuacion
     *
     * @param integer $puntuacion
     * @return Mensaje
     */
    public function setPuntuacion($puntuacion) {
        $this->puntuacion = $puntuacion;

        return $this;
    }

    /**
     * Get puntuacion
     *
     * @return integer
     */
    public function getPuntuacion() {
        return $this->puntuacion;
    }

    /**
     * Set idrecurso
     *
     * @param \Urjavac\RepoBundle\Entity\Recurso $idrecurso
     * @return Mensaje
     */
    public function setIdrecurso(\Urjavac\RepoBundle\Entity\Recurso $idrecurso = null) {
        $this->idrecurso = $idrecurso;

        return $this;
    }

    /**
     * Get idrecurso
     *
     * @return \Urjavac\RepoBundle\Entity\Recurso
     */
    public function getIdrecurso() {
        return $this->idrecurso;
    }

    /**
     * Set autor
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $autor
     * @return Mensaje
     */
    public function setAutor(\Urjavac\RepoBundle\Entity\Usuario $autor = null) {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return \Urjavac\RepoBundle\Entity\Usuario
     */
    public function getAutor() {
        return $this->autor;
    }

}
