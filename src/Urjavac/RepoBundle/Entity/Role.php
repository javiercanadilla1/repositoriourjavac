<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Role
 *
 * @ORM\Table(name="repo_roles")
 * @ORM\Entity(repositoryClass="Urjavac\RepoBundle\Entity\RoleRepository")
 */
class Role implements RoleInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    protected $nombre;

    /**
     *
     * @var type
     * @ORM\ManyToMany(targetEntity="Usuario", mappedBy="roles")

     */
    protected $idUsuario;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Role
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    public function getRole() {
        return $this->getNombre();
    }

    public function __toString() {
        return $this->getRole();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idUsuario = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idUsuario
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $idUsuario
     * @return Role
     */
    public function addIdUsuario(\Urjavac\RepoBundle\Entity\Usuario $idUsuario)
    {
        $this->idUsuario[] = $idUsuario;

        return $this;
    }

    /**
     * Remove idUsuario
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $idUsuario
     */
    public function removeIdUsuario(\Urjavac\RepoBundle\Entity\Usuario $idUsuario)
    {
        $this->idUsuario->removeElement($idUsuario);
    }

    /**
     * Get idUsuario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
