<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Recurso
 *
 * @ORM\Table(name="repo_recurso", indexes={@ORM\Index(name="IDX_B2BB3764FD61E233", columns={"idusuario"}), @ORM\Index(name="IDX_B2BB376410B7C824", columns={"idAsignatura"})})
 * @ORM\Entity(repositoryClass="Urjavac\RepoBundle\Entity\RecursoRepository")
 */
class Recurso {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    protected $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=1000, nullable=false)
     */
    protected $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="enlace", type="string", length=1000, nullable=true)
     */
    protected $enlace;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oficial", type="boolean", nullable=false)
     */
    protected $oficial;

    /**
     * @var boolean
     *
     * @ORM\Column(name="revisado", type="boolean", nullable=false)
     */
    protected $revisado;

    /**
     * @var integer
     *
     * @ORM\Column(name="valor_positivo", type="integer", nullable=true)
     */
    protected $valor_positivo;

    /**
     * @var integer
     *
     * @ORM\Column(name="valor_negativo", type="integer", nullable=true)
     */
    protected $valor_negativo;

    /**
     * @var \Urjavac\RepoBundle\Entity\Curso
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Curso")
     * @ORM\JoinColumn(name="curso", referencedColumnName="id")
     */
    protected $curso;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Usuario", inversedBy="votadoPositivo")
     * @ORM\JoinTable(name="votos_positivos_recurso")
     */
    protected $votosPositivosDe;

    /**
     * @ORM\ManyToMany(targetEntity="Usuario", inversedBy="votadoNegativo")
     * @ORM\JoinTable(name="votos_negativos_recurso")
     */
    protected $votosNegativosDe;

    /**
     * @var \Urjavac\RepoBundle\Entity\Asignatura
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Asignatura")
     * @ORM\JoinColumn(name="idasignatura", referencedColumnName="id")
     */
    protected $idasignatura;

    /**
     * @var \Urjavac\RepoBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     */
    protected $idusuario;

    /**
     * @var \Urjavac\RepoBundle\Entity\Tiporecurso
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Tiporecurso")
     * @ORM\JoinColumn(name="tipo", referencedColumnName="id")
     */
    protected $tipo;

    /**
     * @var \Urjavac\RepoBundle\Entity\Formato
     *
     * @ORM\ManyToOne(targetEntity="Urjavac\RepoBundle\Entity\Formato")
     * @ORM\JoinColumn(name="formato", referencedColumnName="id")
     */
    protected $formato;

    /**
     * @var UploadedFile
     * @Assert\File(maxSize="20M")
     */
    protected $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Recurso
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Recurso
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set enlace
     *
     * @param string $enlace
     * @return Recurso
     */
    public function setEnlace($enlace) {
        $this->enlace = $enlace;

        return $this;
    }

    /**
     * Get enlace
     *
     * @return string
     */
    public function getEnlace() {
        return $this->enlace;
    }

    /**
     * Set oficial
     *
     * @param boolean $oficial
     * @return Recurso
     */
    public function setOficial($oficial) {
        $this->oficial = $oficial;

        return $this;
    }

    /**
     * Get oficial
     *
     * @return boolean
     */
    public function getOficial() {
        return $this->oficial;
    }

    /**
     * Set revisado
     *
     * @param boolean $revisado
     * @return Recurso
     */
    public function setRevisado($revisado) {
        $this->revisado = $revisado;

        return $this;
    }

    /**
     * Get revisado
     *
     * @return boolean
     */
    public function getRevisado() {
        return $this->revisado;
    }

    /**
     * Set curso
     *
     * @param \Urjavac\RepoBundle\Entity\Curso $curso
     * @return Recurso
     */
    public function setCurso(\Urjavac\RepoBundle\Entity\Curso $curso = null) {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso
     *
     * @return \Urjavac\RepoBundle\Entity\Curso
     */
    public function getCurso() {
        return $this->curso;
    }

    /**
     * Set idasignatura
     *
     * @param \Urjavac\RepoBundle\Entity\Asignatura $idasignatura
     * @return Recurso
     */
    public function setIdasignatura(\Urjavac\RepoBundle\Entity\Asignatura $idasignatura = null) {
        $this->idasignatura = $idasignatura;

        return $this;
    }

    /**
     * Get idasignatura
     *
     * @return \Urjavac\RepoBundle\Entity\Asignatura
     */
    public function getIdasignatura() {
        return $this->idasignatura;
    }

    /**
     * Set idusuario
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $idusuario
     * @return Recurso
     */
    public function setIdusuario(\Urjavac\RepoBundle\Entity\Usuario $idusuario = null) {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return \Urjavac\RepoBundle\Entity\Usuario
     */
    public function getIdusuario() {
        return $this->idusuario;
    }

    /**
     * Set tipo
     *
     * @param \Urjavac\RepoBundle\Entity\Tiporecurso $tipo
     * @return Recurso
     */
    public function setTipo(\Urjavac\RepoBundle\Entity\Tiporecurso $tipo = null) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Urjavac\RepoBundle\Entity\Tiporecurso
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set formato
     *
     * @param \Urjavac\RepoBundle\Entity\Formato $formato
     * @return Recurso
     */
    public function setFormato(\Urjavac\RepoBundle\Entity\Formato $formato = null) {
        $this->formato = $formato;

        return $this;
    }

    /**
     * Get formato
     *
     * @return \Urjavac\RepoBundle\Entity\Formato
     */
    public function getFormato() {
        return $this->formato;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    public function getAbsolutePath() {
        return null === $this->enlace ? null : $this->getUploadRootDir() . '/' . $this->enlace;
    }

    public function getWebPath() {
        return null === $this->enlace ? null : '/' . $this->getUploadDir() . '/' . $this->enlace;
    }

    public function getAdminPath() {
        return null === $this->enlace ? null : '/web/' . $this->getUploadDir() . '/' . $this->enlace;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/recursos';
    }

    public function upload() {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // aquí usa el nombre de archivo original pero lo debes
        // sanear al menos para evitar cualquier problema de seguridad
        // move takes the target directory and then the
        // target filename to move to
        $extension = $this->file->guessExtension();
        if (!$extension) {
            // no se puede deducir la extensión
            $extension = 'bin';
        }
        $nombre = (rand(1, 99999) . '.' . $extension);

        $this->getFile()->move($this->getUploadRootDir(), $nombre);

        // set the path property to the filename where you've saved the file
        $this->enlace = $nombre;

        // limpia la propiedad «file» ya que no la necesitas más
        $this->file = null;
    }

    public function removeUpload() {
        $file = $this->getAbsolutePath();
        if (is_file($file)) {
            $unlink = unlink($file);
            if ($unlink) {
                $this->file = null;
            }
        }
    }

    /**
     * Set valor_positivo
     *
     * @param integer $valorPositivo
     * @return Recurso
     */
    public function setValorPositivo($valorPositivo) {
        $this->valor_positivo = $valorPositivo;

        return $this;
    }

    /**
     * Get valor_positivo
     *
     * @return integer
     */
    public function getValorPositivo() {
        return $this->valor_positivo;
    }

    /**
     * Set valor_negativo
     *
     * @param integer $valorNegativo
     * @return Recurso
     */
    public function setValorNegativo($valorNegativo) {
        $this->valor_negativo = $valorNegativo;

        return $this;
    }

    /**
     * Get valor_negativo
     *
     * @return integer
     */
    public function getValorNegativo() {
        return $this->valor_negativo;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->votosPositivosDe = new \Doctrine\Common\Collections\ArrayCollection();
        $this->votosNegativosDe = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add votosPositivosDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $votosPositivosDe
     * @return Recurso
     */
    public function addVotosPositivosDe(\Urjavac\RepoBundle\Entity\Usuario $votosPositivosDe) {
        $this->votosPositivosDe[] = $votosPositivosDe;

        return $this;
    }

    /**
     * Remove votosPositivosDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $votosPositivosDe
     */
    public function removeVotosPositivosDe(\Urjavac\RepoBundle\Entity\Usuario $votosPositivosDe) {
        $this->votosPositivosDe->removeElement($votosPositivosDe);
    }

    /**
     * Get votosPositivosDe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotosPositivosDe() {
        return $this->votosPositivosDe;
    }

    /**
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $usuario
     * @return True or False
     */
    public function hasVotosPositivosDe(Usuario $usuario) {
        return in_array($usuario, $this->votosPositivosDe->toArray(), true);
    }

    /**
     * Add votosNegativosDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $votosNegativosDe
     * @return Recurso
     */
    public function addVotosNegativosDe(\Urjavac\RepoBundle\Entity\Usuario $votosNegativosDe) {
        $this->votosNegativosDe[] = $votosNegativosDe;

        return $this;
    }

    /**
     * Remove votosNegativosDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $votosNegativosDe
     */
    public function removeVotosNegativosDe(\Urjavac\RepoBundle\Entity\Usuario $votosNegativosDe) {
        $this->votosNegativosDe->removeElement($votosNegativosDe);
    }

    /**
     * Get votosNegativosDe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotosNegativosDe() {
        return $this->votosNegativosDe;
    }

    /**
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $usuario
     * @return True or false
     */
    public function hasVotosNegativosDe(Usuario $usuario) {
        return in_array($usuario, $this->votosNegativosDe->toArray(), true);
    }

}
