<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * RecursoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RecursoRepository extends EntityRepository {

    public function countAll() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(r.id)')
                        ->from("UrjavacRepoBundle:Recurso", 'r')
                        ->getQuery()->getSingleScalarResult();
    }

    public function countRevisados() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(r.id)')
                        ->from("UrjavacRepoBundle:Recurso", 'r')
                        ->where("r.revisado = 1")
                        ->getQuery()->getSingleScalarResult();
    }

    public function countNoRevisados() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(r.id)')
                        ->from("UrjavacRepoBundle:Recurso", 'r')
                        ->where("r.revisado = 0")
                        ->getQuery()->getSingleScalarResult();
    }

    public function countOficiales() {
        return $this->getEntityManager()->createQueryBuilder()
                        ->select('COUNT(r.id)')
                        ->from("UrjavacRepoBundle:Recurso", 'r')
                        ->where("r.oficial = 1")
                        ->getQuery()->getSingleScalarResult();
    }

    public function filtrarRecursos(Array $filtros) {
        $createQueryBuilder = $this->getEntityManager()->createQueryBuilder()
                ->select('r')
                ->from("UrjavacRepoBundle:Recurso", 'r')
                ->addOrderBy("r.id", "DESC");
        if ($filtros['radios'] === 0) {
            $createQueryBuilder->where("r.revisado = 1");
        } else if ($filtros['radios'] == 1) {
            $createQueryBuilder->where("r.oficial = 1");
        } else if ($filtros['radios'] == 2) {
            $createQueryBuilder->where("r.revisado = 0");
        } else if ($filtros['radios'] == 3) {
            $createQueryBuilder->where("r.oficial = 0");
        }

        if ($filtros['asignatura'] != null) {
            $createQueryBuilder->andWhere("r.idasignatura = :asignatura");
            $createQueryBuilder->setParameter("asignatura", $filtros['asignatura']);
        }

        if ($filtros['tipo'] != null) {
            $createQueryBuilder->andWhere("r.tipo = :tipo");
            $createQueryBuilder->setParameter("tipo", $filtros['tipo']);
        }

        if ($filtros['curso'] != null) {
            $createQueryBuilder->andWhere("r.curso = :curso");
            $createQueryBuilder->setParameter("curso", $filtros['curso']);
        }

        if ($filtros['formato'] != null) {
            $createQueryBuilder->andWhere("r.formato = :formato");
            $createQueryBuilder->setParameter("formato", $filtros['formato']);
        }

        return $createQueryBuilder->getQuery()->getResult();
    }

}
