<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RespuestaTicket
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RespuestaTicket {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaHora", type="datetime")
     */
    private $fechaHora;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="idUsuario", referencedColumnName="id")
     */
    private $idUsuario;

    /**
     * @var Ticket
     *
     * @ORM\ManyToOne(targetEntity="Ticket", inversedBy="respuestas")
     * @ORM\JoinColumn(name="ticket", referencedColumnName="id", onDelete="CASCADE")
     */
    private $ticket;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return RespuestaTicket
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set fechaHora
     *
     * @param \DateTime $fechaHora
     * @return RespuestaTicket
     */
    public function setFechaHora($fechaHora) {
        $this->fechaHora = $fechaHora;

        return $this;
    }

    /**
     * Get fechaHora
     *
     * @return \DateTime
     */
    public function getFechaHora() {
        return $this->fechaHora;
    }

    /**
     * Set idUsuario
     *
     * @param string $idUsuario
     * @return RespuestaTicket
     */
    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return string
     */
    public function getIdUsuario() {
        return $this->idUsuario;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     * @return RespuestaTicket
     */
    public function setTicket($ticket) {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket() {
        return $this->ticket;
    }

}
