<?php

namespace Urjavac\RepoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Asignatura
 *
 * @ORM\Table(name="repo_asignatura", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_9243D6CEA8255881", columns={"identificador"})})
 * @ORM\Entity(repositoryClass="Urjavac\RepoBundle\Entity\AsignaturaRepository")
 */
class Asignatura {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identificador", type="string", length=6, nullable=false)
     * @ORM\OneToMany(targetEntity="Recurso", mappedBy="idasignatura")
     * @Assert\NotBlank()
     */
    protected $identificador;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=1000, nullable=false)
     * @Assert\NotBlank()
     */
    protected $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="nRecursos", type="integer", nullable=false)
     */
    protected $nrecursos;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Usuario", mappedBy="asignaturasFavoritas")
     *
     */
    protected $favoritaDe;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set identificador
     *
     * @param string $identificador
     * @return Asignatura
     */
    public function setIdentificador($identificador) {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string
     */
    public function getIdentificador() {
        return $this->identificador;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Asignatura
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set nrecursos
     *
     * @param integer $nrecursos
     * @return Asignatura
     */
    public function setNrecursos($nrecursos) {
        $this->nrecursos = $nrecursos;

        return $this;
    }

    /**
     * Get nrecursos
     *
     * @return integer
     */
    public function getNrecursos() {
        return $this->nrecursos;
    }

    public function __toString() {
        return $this->getIdentificador() . " - " . $this->nombre;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->favoritaDe = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add favoritaDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $favoritaDe
     * @return Asignatura
     */
    public function addFavoritaDe(\Urjavac\RepoBundle\Entity\Usuario $favoritaDe) {
        $this->favoritaDe[] = $favoritaDe;

        return $this;
    }

    /**
     * Remove favoritaDe
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $favoritaDe
     */
    public function removeFavoritaDe(\Urjavac\RepoBundle\Entity\Usuario $favoritaDe) {
        $this->favoritaDe->removeElement($favoritaDe);
    }

    /**
     * Get favoritaDe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoritaDe() {
        return $this->favoritaDe;
    }

    /**
     *
     * @param \Urjavac\RepoBundle\Entity\Usuario $usuario
     * @return TRUE or FALSE
     */
    public function hasFavoritaDe(Usuario $usuario) {
        return in_array($usuario, $this->favoritaDe->toArray(), true);
    }

}
