<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdministradoresControllerTest extends WebTestCase
{
    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/administradores/lista');
    }

    public function testEsadministrador()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/administradores/esAdministrador/{numUsuario}');
    }

    public function testNoadministrador()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/administradores/noAdminitrador/{numUsuario}');
    }

    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/administradores/crear');
    }

}
