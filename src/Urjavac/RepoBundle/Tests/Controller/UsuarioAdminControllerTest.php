<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsuarioAdminControllerTest extends WebTestCase
{
    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/usuario/crear');
    }

    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/usuario/lista');
    }

    public function testModificar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/usuario/modificar/{idUsuario}');
    }

    public function testEliminar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/usuario/eliminar/{idUsuario}');
    }

}
