<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TicketControllerTest extends WebTestCase {

    public function testNuevo() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/nuevo');
    }

    public function testEstado() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/estado');
    }

    public function testIndex() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

}
