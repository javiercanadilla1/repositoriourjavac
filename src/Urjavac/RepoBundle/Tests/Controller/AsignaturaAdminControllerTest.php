<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AsignaturaAdminControllerTest extends WebTestCase
{
    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/asignatura/crear');
    }

    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/asignatura/lista');
    }

    public function testModificar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/asignatura/modificar/{numAsignatura}');
    }

    public function testEliminar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/asignatura/eliminar/{numAsignatura}');
    }

}
