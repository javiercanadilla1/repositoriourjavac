<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RolesAdminControllerTest extends WebTestCase
{
    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/rol/crear');
    }

    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/rol/lista');
    }

    public function testModificar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/rol/modificar/{idRol}');
    }

    public function testEliminar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/rol/eliminar/{idRol}');
    }

}
