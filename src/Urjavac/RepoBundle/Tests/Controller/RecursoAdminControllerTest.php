<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecursoAdminControllerTest extends WebTestCase
{
    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/recurso/crear');
    }

    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/recurso/lista');
    }

    public function testModificar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/modificar/{numRecurso}');
    }

    public function testEliminar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/eliminar/{numRecurso}');
    }

}
