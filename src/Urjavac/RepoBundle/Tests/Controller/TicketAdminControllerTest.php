<?php

namespace Urjavac\RepoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TicketAdminControllerTest extends WebTestCase
{
    public function testVista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/vista');
    }

    public function testLista()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/lista');
    }

    public function testEliminar()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/eliminar');
    }

    public function testCambiarestado()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/cambiarEstado');
    }

}
